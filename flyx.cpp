#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>


#define TAB '\t'
#define NL '\n'

std::vector<std::string> tokens;

//////////////////////////////////////////////////////////////////////////////////
//                                     LEXER                                    //
//////////////////////////////////////////////////////////////////////////////////

char lookch;

void getch() {
  std::cin >> std::noskipws >> lookch;
  if (std::cin.eof()) lookch = -1;
}

bool is_whitech(char c) {
  return c == ' ' || c == NL || c == TAB;
}

void skip_whitech() {
  while(is_whitech(lookch)) getch();
}

bool is_alphach(char c) {
  return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_';
}

bool is_digitch(char c) {
  return c >= '0' && c <= '9';
}

std::string decode_num() {
  std::string num = "";
  while (is_digitch(lookch)) {
    num += lookch;
    getch();
  }
  skip_whitech();
  return num;
}

bool is_alnumch(char c) {
  return is_alphach(c) || is_digitch(c);
}

std::string decode_alnum() {
  std::string alnum = "";
  while (is_alnumch(lookch)) {
    alnum += lookch;
    getch();
  }
  skip_whitech();
  return alnum;
}

bool is_eofch(char c) {
  return c == -1;
}

std::string decode_eof() {
  getch();
  skip_whitech();
  return {-1};
}

bool combine_opch(char a, char b) {
  switch(a){
  case '=': return b == '=';
  case '>': return b == '=';
  case '<': return b == '=';
  case '!': return b == '=';
  case '&': return b == '&';
  case '|': return b == '|';
  default: return false;
  }
}

std::string decode_op() {
  std::string op;
  char firstch = lookch;
  getch();
  char secondch = lookch;
  if (combine_opch(firstch, secondch)) {
    op = {firstch, secondch};
    getch();
  } else {
    op = {firstch};
  }
  skip_whitech();
  return op;
}

std::string decode_token() {
  if (is_digitch(lookch)) return decode_num();
  else if (is_alphach(lookch)) return decode_alnum();
  else if (is_eofch(lookch)) return decode_eof();
  else return decode_op();
}

void decode_tokens() {
  std::string lookt;
  do {
    lookt = decode_token();
    tokens.push_back(lookt);
  } while(!is_eofch(lookt[0]));
}

//////////////////////////////////////////////////////////////////////////////////
//                                    PARSER                                    //
//////////////////////////////////////////////////////////////////////////////////

std::string look;
int lookind = 0;

int uc = 1;
std::unordered_map<std::string, int> um;

int l = 1;

std::vector<std::stringstream> emited(1);
#define EMIT emited.reserve(emited.size()+1); emited[emited.size()-1]

void unexpected(std::string s) {
  if (is_eofch(s[0])) s = "End of File";
  std::cerr << "Unexpected " << s << ".\n";
  exit(1);
}

void expected(std::string s) {
  if (is_eofch(s[0])) s = "End of File";
  std::cerr << "Expected " << s << ".\n";
  exit(1);
}

void next_token() {
  look = tokens[std::min(++lookind, (int)tokens.size()-1)];
}

void match(std::string s) {
  if(look == s) next_token();
  else expected(s);
}

bool is_alnum(std::string s) {
  return is_alphach(s[0]);
}

bool is_key(std::string s) {
  if (s == "begin") return true;
  if (s == "end") return true;
  if (s == "print") return true;
  if (s == "printc") return true;
  if (s == "eol") return true;
  if (s == "if") return true;
  if (s == "while") return true;
  if (s == "scan") return true;
  if (s == "scanc") return true;
  return false;
}

bool is_id(std::string s) {
  return is_alnum(s) && !is_key(s);
}

bool is_num(std::string s) {
  return is_digitch(s[0]);
}

void match_id() {
  if (!is_id(look)) expected("Identifier");
  next_token();
}

void match_num() {
  if(!is_num(look)) expected("Number");
  next_token();
}

bool is_eof(std::string s){
  return is_eofch(s[0]);
}

void match_eof() {
  if (!is_eof(look)) expected({-1});
  next_token();
}

int map_id(std::string s) {
  if (um[s] == 0) {
    um[s] = uc++;
  }
  return um[s];
}


void k_scanc(){
  match("scanc");
  EMIT << "scanf(\" %c\", &a);" << NL;
}

void k_scan(){
  match("scan");
  EMIT << "scanf(\"%d\", &a);" << NL;
}


void exp();

void exp_term() {
  if (look == "(") {
    match("(");
    exp();
    match(")");
  } else {
    if (is_id(look)) {
      EMIT << "a = u" << map_id(look) << ";" << NL;
      match_id();
    } else if (is_num(look)) {
      EMIT << "a = " << look << ";" << NL;
      match_num();
    } else if (look == "scan" || look == "scanc") {
      if(look == "scan") k_scan();
      else if(look == "scanc") k_scanc();
    } else unexpected(look);
  }
}

void exp_neg() {
  if (look == "-") {
    match("-");
    exp_neg();
    EMIT << "a = -a;" << NL;
  } else if (look == "+") {
    match("+");
    exp_neg();
  } else {
    exp_term();
  }
}

void div() {
  match("/");
  exp_neg();
  EMIT << "a = s[--sh] / a;" << NL;
}

void mul() {
  match("*");
  exp_neg();
  EMIT << "a = s[--sh] * a;" << NL;
}

void exp_mul() {
  exp_neg();
  while (look == "*" || look == "/") {
    EMIT << "s[sh++] = a;" << NL;
    if (look == "*") mul();
    else if (look == "/") div();
  }
}

void sub () {
  match("-");
  exp_mul();
  EMIT << "a = s[--sh] - a;" << NL;
}

void add() {
  match("+");
  exp_mul();
  EMIT << "a = s[--sh] + a;" << NL;
}

void exp_add() {
  exp_mul();
  while (look == "+" || look == "-") {
    EMIT << "s[sh++] = a;" << NL;
    if (look == "+") add();
    else if (look == "-") sub();
  }
}

void eq() {
  match("==");
  exp_add();
  EMIT << "a = s[--sh] == a;" << NL;
}

void neq() {
  match("!=");
  exp_add();
  EMIT << "a = s[--sh] != a;" << NL;
}

void get() {
  match(">=");
  exp_add();
  EMIT << "a = s[--sh] >= a;" << NL;
}

void let() {
  match("<=");
  exp_add();
  EMIT << "a = s[--sh] <= a;" << NL;
}

void gt() {
  match(">");
  exp_add();
  EMIT << "a = s[--sh] > a;" << NL;
}

void lt() {
  match("<");
  exp_add();
  EMIT << "a = s[--sh] < a;" << NL;
}

void exp_comp() {
  exp_add();
  while (look == "==" || look == "!=" || look == ">=" || look == "<=" || look == ">" || look == "<") {
    EMIT << "s[sh++] = a;" << NL;
    if(look == "==") eq();
    else if(look == "!=") neq();
    else if(look == ">=") get();
    else if(look == "<=") let();
    else if(look == ">") gt();
    else if(look == "<") lt(); 
  }
}

void andop() {
  match("&&");
  exp_comp();
  EMIT << "a = s[--sh] && a;" << NL;
}

void exp_and() {
  exp_comp();
  while (look == "&&") {
    EMIT << "s[sh++] = a;" << NL;
    andop();
  }
}

void orop() {
  match("||");
  exp_and();
  EMIT << "a = s[--sh] || a;" << NL;
}

void exp_or() {
  exp_and();
  while (look == "||") {
    EMIT << "s[sh++] = a;" << NL;
    orop();
  }
}

void exp_ass() {
  if(tokens[lookind + 1] != "=") {
    exp_or();
    return;
  }
  int id = map_id(look);
  match_id();
  match("=");
  exp();
  EMIT << "u" << id << " = a;" << NL;
}

void exp() {
  exp_ass();
}

void k_block();
void k_line();
void k_if();
void k_while();
void k_print();
void k_printc();
void k_eol();

void k_eol(){
  match("eol");
  EMIT << "printf(\"\\n\");" << NL;
}

void k_printc(){
  match("printc");
  exp();
  EMIT << "printf(\"%c\", a);" << NL;
}

void k_print(){
  match("print");
  exp();
  EMIT << "printf(\"%d\", a);" << NL;
}

void k_while(){
  int wl = l++;
  match("while");
  EMIT << "wb" << wl << ":" << NL;
  exp();
  EMIT << "if (!a) goto we" << wl << ";" << NL;
  k_block();
  EMIT << "goto wb" << wl << ";" << NL;
  EMIT << "we" << wl << ":" << NL;
}

void k_if() {
  int ifl = l++;
  match("if");
  exp();
  EMIT << "if (!a) goto if" << ifl << ";" << NL;
  k_block();
  EMIT << "if" << ifl << ":" << NL;
}

void k_line() {
  if (look == "begin") k_block();
  else if (look == "if") k_if();
  else if (look == "while") k_while();
  else if (look == "print") k_print();
  else if (look == "printc") k_printc();
  else if (look == "eol") k_eol();
  else exp();
}

void k_block() {
  if (look != "begin") {
    k_line();
    return;
  }
  match("begin");
  while(look != "end") k_line();
  match("end");
}

void init() {
  getch();
  skip_whitech();
  decode_tokens();
  look = tokens[lookind];
}

void initout() {
  std::cout << "#include <stdio.h>\n";
  std::cout << "#define STACK_SIZE 1000\n";
  std::cout << "int s[STACK_SIZE], sh = 0, a = 0;\n";
  if (uc > 1) std::cout << "int";
  for (int i = 1; i < uc; i++) std::cout << " u" << i << " = 0" << (i == uc - 1 ? ';' : ',');
  if(uc > 1) std::cout << NL;
  std::cout << "int main() {\n\n";
}

void midout() {
  for (int i = 0; i < (int)emited.size(); i++) {
    std::cout << emited[i].str();
  } 
}

void endout() {
  std::cout << "return 0;\n";
  std::cout << "}\n";
}

int main() {
  init();
  k_block();
  match_eof();
  initout();
  midout();
  endout();
}

/*
begin
air2 = air = scan - 1
char = scanc
leaves = 1
while air >= 0 begin
	i = 0
	while i < air begin
		printc 32
		i = i + 1
	end
	i = 0
	while i < leaves begin
		printc char
		i = i + 1
	end
	eol
	leaves = leaves + 2
	air = air - 1
end	
while air2 > 0 begin
	printc 32
	air2 = air2 - 1
end
printc char
eol
end
*/
