CC := g++
CF := -Wall -Werror

flyx: flyx.cpp
	${CC} -o flyx flyx.cpp ${CF}

install: flyx
	cp flyx /usr/bin/flyx

clean:
	rm flyx
	rm /usr/bin/flyx





